import sys
import subprocess
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["wx"], "includes": ["queue"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None

if sys.platform == "win32":
    base = "Win32GUI"

setup(name="BrainHurt",
      version="0.1",
      description="Konson lanipeli.",
      options={"build_exe": build_exe_options},
      executables=[Executable("main.py", base=base)])


