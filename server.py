import time
import operator
import wx
import queue
from PodSixNet.Server import Server
from PodSixNet.Channel import Channel
# from PodSixNet.Connection import connection
from constants import *
from collections import Counter
from logger import log

TAG = 'SERVER'


class TimerError(Exception):
    def __init__(self, message):
        self.message = message


class ClientChannel(Channel):
    """
    This is the server representation of a single connected client.
    """

    def __init__(self, *args, **kwargs):
        Channel.__init__(self, *args, **kwargs)
        self.nick = "anonymous"
        self.score = 0
        self.round_results = {}
        self.action = ACTION_NONE
        self.validated_guesses = {}

    def __repr__(self):
        return f"<ClientChannel {self.nick} {self.addr[0]} {self.addr[1]}>"

    def Close(self):
        self._server.delete_player(self)

    ##############################
    # Network specific callbacks #
    ##############################

    def Network_message(self, data):
        self._server.send_to_all({"action": "message", "message": data['message'], "who": self.nick})

    def Network_nick(self, data):
        log(TAG, f"received nick change, data: {data}")
        self.nick = data['nick']
        self._server.send_nick_list()

    def Network_round_results(self, data):
        log(TAG, f"received round results for {self.nick}, data: {data}")
        self.round_results = data['round_results']

    def Network_user_action(self, data):
        # log(TAG, f"received action {data['user_action']} from {self.nick}")
        # Tämän lukee server thread x millisekunnin välein
        self.action = int(data['user_action'])
        # self._server.send_nick_list()


class MyServer(Server):
    channelClass = ClientChannel

    def __init__(self, localaddr, main_window, message_queue, game_settings):
        Server.__init__(self, localaddr=localaddr)
        self.message_queue = message_queue
        self.window = main_window
        self.running = False
        self.killed_by_main = False
        self.players = []
        self.round_state = None
        self.round_number = 1
        self._timer_started_at = 0  # 0 = ei timeriä käynnissä
        self._timer_time = 0
        self._timer_label = ''
        log(TAG, f"__init__: _game_settings: {game_settings}")
        self._game_settings = game_settings
        self.wait_time_after_round_end_ms = 1000 # kuinka kauan odotetaan clienteiltä vastausta
        self._last_action_update = 0
        # log(TAG, f"init - message queue: {self.message_queue}")

    def Connected(self, channel, addr):
        log(TAG, f"New connection: {addr}")  # {addr}")
        channel.score = self._game_settings['starting_points']
        self.players.append(channel)
        # channel.Send("Welcome!")
        # self.send_nick_list()

    def run(self):
        self.running = True
        log(TAG, f": running")
        while self.running:
            try:
                message = self.message_queue.get(block=False)
                self.handle_message(message)
            except queue.Empty:
                pass

            # Jos roundi käynnissä niin updatetaan actionin x ms:n välein
            now = time.time()
            if self.round_state == ROUND_IN_PROGRESS:
                if now - self._last_action_update > SERVER_ACTION_UPDATE_INTERVAL_MS / 1000:
                    self.send_nick_list()
                    self._last_action_update = now

            if self._timer_started_at != 0:  # timer in progress
                if self.timer_finished():
                    if self.round_state == ROUND_PRE_COUNTDOWN:
                        self.start_round()
                    elif self.round_state == ROUND_IN_PROGRESS:
                        self.end_round()
                    elif self.round_state == ROUND_FINISHED:
                        self.handle_round_results()

            self.Pump()
            time.sleep(0.001)
        if not self.killed_by_main:
            self.send_to_main(action='thread_info', data=INFO_SERVER_TERMINATED)

        try:
            self.send_to_all({'action': 'server_disconnected', 'data': INFO_SERVER_TERMINATED})
            self.Pump()
        except Exception as e:
            log(TAG, f"Exception when tread shutting down and sending info about this to clients")
            log(TAG, f"Konso: fix this")
            log(TAG, e)
            log(TAG, f"Press ENTER to continue...")
            input()
        time.sleep(0.1)
        log(TAG, "SERVER: terminated")
        # self.close_all()

    def send_to_main(self, action='message', data=''):
        wx.CallAfter(self.window.message_receiver, {action: data})

    def handle_message(self, message):
        """ Handles messages from main thread """
        if 'command' in message:
            if message['command'] == COMMAND_KILL_SERVER:
                log(TAG, f"terminating (command from main)")
                self.killed_by_main = True
                self.running = False
        elif 'round_state' in message:
            if message['round_state'] == ROUND_PRE_COUNTDOWN:
                self.start_round_pre_countdown()
        elif 'settings' in message:
            log(TAG, f"got game settings: {message['settings']}")
            self._game_settings = message['settings']

    def start_round_pre_countdown(self):
        log(TAG, f"starting pre-round countdown for round {self.round_number}")
        self.round_state = ROUND_PRE_COUNTDOWN
        self.send_to_all({'action': 'round_state', 'round_state': ROUND_PRE_COUNTDOWN})
        self.send_to_all({'action': 'settings', 'settings': self._game_settings})
        for p in self.players:
            if self.round_number == 1:
                log(TAG, f"first round - setting score for {p.nick}: {self._game_settings['starting_points']}")
                p.score = self._game_settings['starting_points']
            p.round_results = {}
        self.send_player_scores()
        self.send_nick_list()  # also has score info
        self.start_timer(self._game_settings['round_pre_countdown_time'], 'round pre-countdown')

    def start_round(self):
        log(TAG, f"starting round")
        self.round_state = ROUND_IN_PROGRESS
        self.send_to_all({'action': 'round_state', 'round_state': ROUND_IN_PROGRESS})
        self.start_timer(self._game_settings['round_time'], 'round')

    def end_round(self):
        log(TAG, f"ending round - wait for results from clients")
        self.round_state = ROUND_FINISHED
        self.send_to_all({'action': 'round_state', 'round_state': ROUND_FINISHED})
        self.start_timer(self.wait_time_after_round_end_ms / 1000, 'wait for client results')
        self.send_nick_list()  # ettei enää näy user actionit

    def handle_round_results(self):
        log(TAG, f"handling round {self.round_number} results")
        highest_winning = {}
        highest_not_winning = {}
        results_for_clients = {}

        min_allowed_guess = 1  # HOX! NOT READ FROM SETTINGS HERE!
        max_allowed_guess = int(len(self.players) * self._game_settings['guessable_numbers_per_player'])

        for level in range(LEVELS):
            log(TAG, f"LEVEL {level}")

            # Phase 1 - check guess validity
            #   * must be int
            #   * player must have enough points
            #   * must be in allowed range
            #
            # Currently player gets refund for invalid guesses - change?

            buy_in_price = self._game_settings['buy_in_for_level'][level]
            unordered_results = {}  # is this used at all?
            results_for_clients[level] = {}
            guesses = []

            for p in self.players:
                try:
                    guess = int(p.round_results[level])
                    if min_allowed_guess <= guess <= max_allowed_guess and p.score >= buy_in_price:
                        p.score -= buy_in_price
                        unordered_results[p] = guess
                        guesses.append(guess)
                        p.validated_guesses[level] = guess
                        results_for_clients[level][p.nick] = guess
                except (KeyError, ValueError):
                    pass

            # Phase 2 - check biggest winning and biggest losing guess
            # Different way for level 0 / other levels

            number_counts = Counter(guesses)
            guesses.sort(reverse=True)

            if level == 0:

                for guess in guesses:
                    # log(TAG, f"Handling guess {guess}...")
                    if number_counts[guess] > 1:
                        # log(TAG, f"More than 1 counts - can't be a winner...")
                        if highest_not_winning.get(level) is None:
                            # log(TAG, f"No previous biggest loser - setting")
                            highest_not_winning[level] = guess
                    else:
                        # log(TAG, f"Only 1 count - winner candidate")
                        if highest_winning.get(level) is None:
                            # log(TAG, f"No previous winner, winner found!")
                            highest_winning[level] = guess
                        else:
                            # log(TAG, f"Winner already found before - biggest loser candidate")
                            if highest_not_winning.get(level) is None:
                                # log(TAG, f"No previous biggest loser - setting")
                                highest_not_winning[level] = guess

                    # Found results, no need to continue
                    if highest_not_winning.get(level) is not None and highest_winning.get(level) is not None:
                        # log(TAG, f"Biggest loser and winner found, continue...")
                        break

            # Phase 2 for levels 1->
            else:
                # In next levels there can be more than one winner per level
                for guess in number_counts:
                    if guess == highest_not_winning.get(level - 1) and highest_winning.get(level) is None:
                        highest_winning[level] = guess

                    else:
                        if highest_not_winning.get(level) is None:
                            highest_not_winning[level] = guess

                    if highest_winning.get(level) is not None and highest_not_winning.get(level) is not None:
                        break

            log(TAG, f"round results for level {level}:") #
            # log(TAG, f"Results: {unordered_results}")
            log(TAG, f"Guesses: {guesses}")
            # log(TAG, f"Number counts: {number_counts}")
            log(TAG, f"Highest winning: {highest_winning.get(level)}")
            log(TAG, f"Highest not winning: {highest_not_winning.get(level)}")

            results_for_clients[level]['winning'] = highest_winning.get(level)
            results_for_clients[level]['highest_not_winning'] = highest_not_winning.get(level)

            # Phase 3
            #   * give points to winners

            points_for_win = self._game_settings['score_for_level'][level]
            for p in self.players:
                if p.validated_guesses.get(level) is not None and \
                        p.validated_guesses.get(level) == highest_winning.get(level):
                    log(TAG, f"Award {points_for_win} points to {p.nick} with guess {p.validated_guesses[level]}")
                    p.score += points_for_win

        # Phase 4 - send new scores to players and update nick list
        self.send_player_scores()
        self.send_nick_list()
        self.send_to_all({'action': 'round_results', 'round_results': results_for_clients})
        self.round_number += 1
        log(TAG, self._game_settings)
        if self.round_number <= self._game_settings['rounds']:
            self.send_to_all({'action': 'round_number', 'round_number': self.round_number})
        else:
            self.all_rounds_finished()

    def all_rounds_finished(self):
        log(TAG, f"all rounds finished")
        self.sort_players_by_score()
        log(TAG, f"WINNER IS {self.players[0].nick}!!")
        self.send_to_all({'action': 'final_results', 'final_results': {p.nick: p.score for p in self.players}})
        self.round_number = 1

    def start_timer(self, seconds, label='no explanation'):
        log(TAG, f"starting timer ({label}) for {seconds} seconds")
        self._timer_time = seconds
        self._timer_started_at = time.time()
        self._timer_label = label

    def timer_finished(self):
        """ Checks whether timer is finished. Returns True/False. If finished, also reset the timer. """
        if self._timer_started_at == 0:
            raise TimerError("Can't check timer that hasn't been started")
        elif time.time() - self._timer_started_at > self._timer_time:
            log(TAG, f"timer ({self._timer_label}) finished (took {time.time() - self._timer_started_at} seconds)")
            self._timer_started_at = 0
            return True
        else:
            return False

    def delete_player(self, player):
        self.players.remove(player)
        self.send_nick_list()

    def send_player_scores(self):
        for p in self.players:
            p.Send({'action': 'score', 'score': p.score})

    def send_nick_list(self):
        """ Also sends player actions if round in progress """
        nicklist = []
        self.sort_players_by_score()
        for p in self.players:
            if self.round_state == ROUND_IN_PROGRESS:
                action = f" {ACTION_TO_TEXT[p.action]}"
            else:
                action = ""
            nicklist.append(f"{p.score} - {p.nick}{action}")
        self.send_to_all({"action": "nick_list", "nick_list": nicklist})

    def sort_players_by_score(self):
        self.players.sort(key=operator.attrgetter('score'), reverse=True)

    def send_to_all(self, data):
        # log(TAG, f"sending to all: {data}")
        [p.Send(data) for p in self.players]


if __name__ == '__main__':
    myserver = MyServer(localaddr=('localhost', PORT))
    while True:
        myserver.Pump()
        myserver.timer_finished()
        time.sleep(0.01)
