import json
import threading
# cuases circular import:
# from logger import log

TAG = 'SETTINGS'


# TODO - make Settings a singleton
class Settings:
    __settings_singleton = None

    def __init__(self):
        self.settings_filename = 'settings.json'

        # Defaults here - used if no existing settings can be loaded

        #######################
        # PERSISTENT SETTINGS #
        #######################
        self._nick = ""             # autosave with property - thread-safe
        self._server_address = ""   # autosave with property
        self._round_pre_countdown_time = 5  # autosave with property
        self._round_time = 30       # seconds - autosave with property
        self._starting_points = 10  # autosave with property
        self._rounds = 5            # autosave with property
        # TODO: tweak these
        self.buy_in_for_level = [0, 1, 2, 4, 8]
        self.score_for_level = [1, 3, 6, 12, 24]
        self.guessable_numbers_per_player = 2

        self._logging = True         # autosave with property

        # These setting values will be saved to file
        self.PERSISTENT_SETTINGS = ['_nick', '_server_address', '_round_pre_countdown_time', '_round_time',
                                    '_starting_points', '_rounds', 'buy_in_for_level', 'score_for_level',
                                    'guessable_numbers_per_player', '_logging']

        ###########################
        # NON-PERSISTENT SETTINGS #
        ###########################
        self._nick_lock = threading.Lock()
        self.score = self._starting_points
        self.port = 8302
        self.own_ip = ""
        self.min_allowed_guess = 1  # DO NOT CHANGE THIS - server doesn't recognize if changed here
        self.max_allowed_guess = 1  # this will change while running
        self.action_lifetime_ms = 2000

        # Load settings from file on init
        self.load()

    def load(self):
        try:
            with open(self.settings_filename) as f:
                read_settings = json.load(f)
                for item in self.PERSISTENT_SETTINGS:
                    # TODO: tee nätimmin ja turvallisemmin
                    expression = f"self.{item} = read_settings['{item}']"
                    exec(expression, globals(), locals())

        except (OSError, json.JSONDecodeError, KeyError):
            # Create new settings file with default values
            self.save()

    def save(self):
        try:
            with open(self.settings_filename, 'w') as f:
                save_dict = {}
                for key, value in self.__dict__.items():
                    if key in self.PERSISTENT_SETTINGS:
                        save_dict[key] = value
                json.dump(save_dict, f, indent=4)
        except OSError as e:
            print(f"Error writing settings file {self.settings_filename}\n{e}")

    def export_game_settings(self):
        """ Export game settings that server needs to send to clients as dict """
        return {
            'round_pre_countdown_time': self.round_pre_countdown_time,
            'round_time': self.round_time,
            'rounds': self.rounds,
            'starting_points': self.starting_points,
            'buy_in_for_level': self.buy_in_for_level,
            'score_for_level': self.score_for_level,
            'guessable_numbers_per_player': self.guessable_numbers_per_player,
        }

    def import_game_settings(self, game_settings_dict):
        """ Import game settings that server has sent """
        print(f"SETTINGS: import_game_settings called with {game_settings_dict}")
        for key, value in game_settings_dict.items():
            setattr(self, key, value)

    def _get_nick(self):
        with self._nick_lock:
            return self._nick

    def _set_nick(self, nick):
        with self._nick_lock:
            self._nick = nick
            self.save()

    def _get_server_address(self):
        return self._server_address

    def _set_server_address(self, addr):
        self._server_address = addr
        self.save()

    def _get_round_time(self):
        return self._round_time

    def _set_round_time(self, time):
        self._round_time = time
        self.save()

    def _get_starting_points(self):
        return self._starting_points

    def _set_starting_points(self, points):
        self._starting_points = points
        self.save()

    def _get_round_pre_countdown_time(self):
        return self._round_pre_countdown_time

    def _set_round_pre_countdown_time(self, time):
        self._round_pre_countdown_time = time
        self.save()

    def _get_rounds(self):
        return self._rounds

    def _set_rounds(self, rounds):
        # log(TAG, f"Settings._set_rounds called with {rounds}")
        self._rounds = rounds
        self.save()

    def _get_logging(self):
        return self._logging

    def _set_logging(self, logging):
        self._logging = logging
        self.save()

    nick =                      property(_get_nick, _set_nick)
    server_address =            property(_get_server_address, _set_server_address)
    round_pre_countdown_time =  property(_get_round_pre_countdown_time, _set_round_pre_countdown_time)
    round_time =                property(_get_round_time, _set_round_time)
    starting_points =           property(_get_starting_points, _set_starting_points)
    rounds =                    property(_get_rounds, _set_rounds)
    logging =                   property(_get_logging, _set_logging)


if __name__ == '__main__':
    settings = Settings()
    print(settings.nick)
    settings.nick = "Pertti"
