import sys
import wx
import datetime
import traceback
from constants import *


def log_crashes(func, *args, **kwargs):
    """ Decorator. Run the given function. If crashes occur, log them to file. """
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            now_string = str(datetime.datetime.now())

            try:
                with open(CRASHDUMP_FILE, 'a') as f:
                    f.write(f"\n\n--------------------------------------\n{now_string}\
                    \n\n{traceback.format_exc()}\n--------------------------------------")
                    file_write_string = f"Tiedot tallennettiin tiedostoon {CRASHDUMP_FILE}"
            except IOError as e:
                file_write_string = f"Tietojen tallennus tiedostoon {CRASHDUMP_FILE} ei onnistunut."

            error_message = f"Tapahtui isohko virhe\n\n{file_write_string}\n\nPliis kerro Konsolle:\n\n{traceback.format_exc()}"
            try:
                wx.MessageBox(
                    error_message,
                    "Virhe :(", style=wx.ICON_ERROR | wx.OK)
            except wx._core.PyNoAppError:
                print(error_message)

            sys.exit(-1)
    return wrapper


if __name__ == '__main__':
    @log_crashes
    def divide(first, second):
        return first / second

    print(divide(1, 2))
    print(divide(1, 0))


