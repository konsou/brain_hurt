import wx
import socket
import server
import client
import threading
import sys
import queue
import winsound
import time
from gui_part2 import GuiPart2
from settings import Settings
from crash_logger import log_crashes
from constants import *
from collections import OrderedDict
from logger import log

# TODO: timerit toimimaan clientin päässä nätisti ettei katka peli kesken vaikka näyttäisi olevan vielä aikaa jäljellä

TAG = 'MAIN'


@log_crashes
def run():
    app = wx.App(False)
    frame = MainMenuWithLogic(None)
    frame.Show(True)
    app.MainLoop()


class MainMenuWithLogic(GuiPart2):
    def __init__(self, parent):
        GuiPart2.__init__(self, parent)
        log(TAG, '-----------------------------------')
        log(TAG, 'PROGRAM STARTING')
        self.settings = Settings()
        self.textNick.Value = self.settings.nick

        # Oman IP:n selvittäminen
        self.settings.own_ip = socket.gethostbyname(socket.gethostname())
        self.labelOwnIP.Label = self.settings.own_ip
        # log(f"'{self.settings.server_address}'")
        if len(self.settings.server_address) > 0:
            # Otetaan serverin osoite asetuksista jos siellä on
            self.textServerAddress.Value = self.settings.server_address
        else:
            # Jos serverin osoitetta ei löydy asetuksista niin laitetaan siihen oman IP:n alkuosa
            # Esim. jos oma IP on "192.168.1.3" -> "192.168.1."
            self.textServerAddress.Value = f"""{".".join(self.settings.own_ip.split(".")[:-1])}."""

        self.round_in_progress = 0
        self.round_started_at = None
        self.round_number = 1
        self.timer_value = 0
        self.labelTimeLeft.Label = f"Roundi {self.round_number} alkaa: {self.timer_value}"
        self.current_state = STATE_LOCAL_IN_MENU  # not used?
        self.round_state = ROUND_NOT_STARTED
        self.user_action = ACTION_NONE
        self._user_action_last_update = 0

        self.update_score(self.settings.starting_points)
        self.enable_play_area(False)

        self.round_timer_message = ''

        # Verkkohässäkät
        self.server_object = None
        self.server_thread = None
        self.server_message_queue = queue.Queue()
        self.client_object = None
        self.client_thread = None
        self.client_message_queue = queue.Queue()

        self.textRounds.Value = str(self.settings.rounds)
        self.labelRounds.Disable()
        self.textRounds.Disable()
        self.buttonStart.Disable()

        self.timerGuiUpdate.Start(100)

    def change_nick(self, event=None):
        nick = self.textNick.Value
        log(f"MAIN", f"Change nick to {nick}")
        self.settings.nick = nick
        self.buttonChangeNick.Disable()
        if self.client_object is not None:
            self.send_to_client(action='nick', data=nick)

    def connect(self, event=None, server_address=None):
        # Yhdistäminen
        if self.client_object is None:
            # Jos käyttäjä on kirjoittanut uuden nickin mutta ei ole painanut "Vaihda nick" niin
            # tehdään se käyttäjän puolesta
            if self.textNick.Value != self.settings.nick:
                self.change_nick()

            if server_address is None:
                server_address = self.textServerAddress.Value
            self.settings.server_address = server_address
            self.client_message_queue = queue.Queue()
            self.client_object = client.Client(server_address, self.settings.port,
                                               main_window=self, message_queue=self.client_message_queue)
            self.client_thread = threading.Thread(target=self.client_object.Loop, name='client')
            try:
                self.client_thread.start()
                self.buttonConnect.Label = "Poistu pelistä"
                # Jos lokaali serveri ei pystyssä niin disabloidaan hostausnappi
                if self.server_object is None:
                    self.buttonHost.Disable()
            except OSError:
                log(f"MAIN", f"error connecting to {server_address}")
                self.kill_client_thread()
        # Yhteyden katkaisu
        else:
            self.buttonConnect.Label = "Yhdistä"
            self.buttonHost.Enable()
            self.kill_client_thread()

    def host_game(self, event):
        # TODO: ei voi uudelleenhostia jos on hostinut ja lopettanut hostimisen
        # Jos ei vielä ole serveriä pyörimässä niin aloitetaan
        if self.server_object is None:
            self.buttonConnect.Disable()
            self.buttonStart.Enable()
            self.buttonHost.Label = "Lopeta hostaus"
            self.labelRounds.Enable()
            self.textRounds.Enable()
            self.server_message_queue = queue.Queue()
            # localaddr 0.0.0.0 = bound to all interfaces
            self.server_object = server.MyServer(localaddr=('0.0.0.0', self.settings.port),
                                                 main_window=self, message_queue=self.server_message_queue,
                                                 game_settings=self.settings.export_game_settings()
                                                 )

            self.server_thread = threading.Thread(target=self.server_object.run, name='server')
            self.server_thread.start()
            self.connect(server_address='localhost')

        # Jos on serveri pyörimässä niin lopetetaan se
        else:
            self.buttonHost.Label = "Hosti peli"
            self.buttonConnect.Enable()
            self.buttonStart.Disable()
            self.kill_server_thread()

    def message_receiver(self, message):
        """ Receives messages from client and server threads """
        # log(f"MAIN", f"got this message: {message}")
        if 'thread_info' in message:
            if message['thread_info'] == INFO_CLIENT_TERMINATED:
                self.kill_client_thread(msg_source='thread')
            elif message['thread_info'] == INFO_SERVER_TERMINATED:
                self.kill_server_thread(msg_source='thread')

        if 'nick_list' in message:
            self.update_multi_list(message['nick_list'])
            # Infer number of players from length of nick list
            # Update max allowed guess from that number
            self.settings.max_allowed_guess = int(len(message['nick_list']) * self.settings.guessable_numbers_per_player)
            # self.update_allowed_numbers_text()

        if 'round_state' in message:
            if message['round_state'] == ROUND_PRE_COUNTDOWN:
                self.start_round_pre_countdown()
            elif message['round_state'] == ROUND_IN_PROGRESS:
                self.start_round()
            elif message['round_state'] == ROUND_FINISHED:
                self.round_finished()

        if 'round_number' in message:
            self.update_round_number(message['round_number'])

        if 'settings' in message:
            self.update_game_settings(message['settings'])

        if 'score' in message:
            self.update_score(message['score'])

        if 'round_results' in message:
            self.show_round_results(message['round_results'])

        if 'final_results' in message:
            self.show_final_results(message['final_results'])

    def show_round_results(self, results):

        results_string = f"TULOKSET\n\n"
        for level, results_for_level in results.items():
            results_string += f"TASO {level + 1}\nVoittanut arvaus: {results_for_level['winning']}\n" +\
                              f"Korkein ei-voittanut arvaus: {results_for_level['highest_not_winning']}\n\n" +\
                              f"Arvaukset:\n"

            for key, value in results_for_level.items():
                if key != 'winning' and key != 'highest_not_winning':
                    results_string += f"{key}: {value}"
                    if value == results_for_level['winning']:
                        results_string += f"  - VOITTAJA! (+{self.settings.score_for_level[level]} pts)"
                    results_string += "\n"
            results_string += "------------------------\n"
        # Sort by value
        # ordered_results = OrderedDict(sorted(results.items(), key=lambda item: (item[1], item[0]), reverse=True))
        # first = True
        # for key, value in ordered_results.items():
        #     if first:
        #         first = False
        #         results_string += f"VOITTAJA ON {key.upper()}!!\n\n"
        #     results_string += f"{key}: {value}"
        wx.MessageBox(results_string, 'Tulokset')

    def show_final_results(self, results):
        # Sort by value
        ordered_results = OrderedDict(sorted(results.items(), key=lambda item: (item[1], item[0]), reverse=True))
        results_string = f"TULOKSET\n\n"
        first = True
        for key, value in ordered_results.items():
            if first:
                first = False
                results_string += f"VOITTAJA ON {key.upper()}!!\n\n"
            results_string += f"{key}: {value}\n"
        wx.MessageBox(results_string, 'Tulokset')

        # Reset round number and score
        self.update_round_number(1)
        # self.settings.score = self.settings.starting_points

    def update_game_settings(self, new_settings):
        log(f"MAIN", f"updating game settings")
        self.settings.import_game_settings(new_settings)
        self.update_allowed_numbers_text()

    def update_score(self, score):
        log(f"MAIN", f"update own score to {score}")
        self.settings.score = score
        self.labelPoints.Label = f"OMAT PISTEET: {self.settings.score}"
        if self.round_state == ROUND_IN_PROGRESS:
            self.enable_disable_game_text_fields_by_score()

    def update_multi_list(self, items):
        self.multiList.Clear()
        self.multiList.Append(items)

    def update_round_number(self, number):
        log(f"MAIN", f"update round number to {number}")
        self.round_number = number
        self.buttonStart.Label = f"Aloita {number}. kierros"

    # def update_time_left_label(self, time_left):
    #    self.labelTimeLeft.Label = f"{self.round_timer_message}{self.timer_value}"

    def enable_join_button(self):
        """ Enables join button and resets its text to "Join game" """
        self.buttonConnect.Enable()
        self.buttonConnect.Label = "Yhdistä"

    def enable_host_button(self):
        """ Enables host button and resets its text to "Host a game" """
        self.buttonHost.Enable()
        self.buttonHost.Label = "Hosti peli"

    def kill_client_thread(self, msg_source='main'):
        if msg_source == 'main':
            self.client_message_queue.put({'command': COMMAND_KILL_CLIENT})

        self.client_object = None
        self.client_thread = None
        self.enable_host_button()
        self.enable_join_button()
        self.update_multi_list([])

    def kill_server_thread(self, msg_source='main'):
        if msg_source == 'main':
            self.server_message_queue.put({'command': COMMAND_KILL_SERVER})

        self.server_object = None
        self.server_thread = None
        self.enable_host_button()
        self.enable_join_button()
        self.labelRounds.Enable(False)
        self.textRounds.Enable(False)
        self.update_multi_list([])

    def send_to_client(self, action='', data=''):
        if self.client_object is None:
            raise threading.ThreadError("Trying to send data to client thread but it's not started")
        else:
            self.client_message_queue.put({action: data})

    def send_to_server(self, action='', data=None):
        if self.server_object is None:
            raise threading.ThreadError("Trying to send data to server thread but it's not started")
        else:
            self.server_message_queue.put({action: data})

    def start_round_button_pressed(self, event):
        """
        Lähettää serverithreadiin tiedon että nyt alkaa pre-countdown
        Serverithread hoitaa ajastukset ja viestien lähettämisen clienteille
        """
        self.send_to_server(action='settings', data=self.settings.export_game_settings())
        self.send_to_server(action='round_state', data=ROUND_PRE_COUNTDOWN)

    def start_round_pre_countdown(self):
        log(f"MAIN", f"start round pre-countdown")
        # self.measure_time(message='pre-countdown')
        self.round_state = ROUND_PRE_COUNTDOWN
        self.start_timer(self.settings.round_pre_countdown_time, f"Roundi {self.round_number} alkaa: ")

    def start_round(self):
        # self.measure_time('stop')
        # self.measure_time(message='round')
        log(f"MAIN", f"start round")
        self.enable_play_area()
        self.enable_disable_game_text_fields_by_score()
        self.round_state = ROUND_IN_PROGRESS
        self.start_timer(self.settings.round_time, f"Roundi {self.round_number} päättyy: ")

    def round_finished(self):
        # self.measure_time('stop')
        log(f"MAIN", f"round finished")
        self.labelTimeLeft.Disable()
        self.enable_play_area(False)
        self.round_state = ROUND_FINISHED
        self.send_round_results()
        # Clear checkboxes AFTER sending results because this triggers things
        self.clear_play_area_checkboxes()
        # server waits for a while (1s?) for response from clients - done
        # server validates client responses - done partially
        # server sends round results to clients
        # client updates results to main

    def send_round_results(self):
        results = {}
        for level in range(LEVELS):
            if self.gameCheckBoxes[level].IsChecked():
                results[level] = self.gameTextFields[level].Value

        self.send_to_client(action='round_results', data=results)

    def start_timer(self, seconds, message=''):
        self.labelTimeLeft.Enable()
        self.timer_value = seconds
        self.round_timer_message = message
        self.labelTimeLeft.Label = f"{self.round_timer_message}{self.timer_value}"
        self.timer1S.Start(1000)

    def timer_1s_tick(self, event):
        """ Timer is cosmetic only - true timer happens in server thread """
        self.timer_value -= 1
        if self.timer_value < 0:
            # log(f"MAIN", f"timer ran out!")
            self.timer1S.Stop()
            self.labelTimeLeft.Disable()
        else:
            self.labelTimeLeft.Label = f"{self.round_timer_message}{self.timer_value}"

    def timer_round_tick(self, event):
        log(f"MAIN", f"round finished!")
        winsound.MessageBeep()
        # self.timer1S.Stop()
        self.labelTimeLeft.Label = '0.0'
        self.round_in_progress = 0

    # TODO: Timeri sittenkin tämän avulla että on varmempi
    # def update_gui(self, event):
    #     if self.round_in_progress:
    #         self.labelTimeLeft.Label = f"{self.settings.round_time - (time.time() - self.round_started_at):.1f}"

    def enable_play_area(self, enable=True):
        """ Enable or disable the game area """
        for item in self.gameTextFields:
            item.Enable(enable)
        for item in self.gameCheckBoxes:
            item.Enable(enable)
        for item in self.gameInfoLabels:
            item.Enable(enable)
        for item in self.gameInfoAllowedNumbers:
            item.Enable(enable)
        for item in self.gameScoreLabels:
            item.Enable(enable)
        self.labelPoints.Enable(enable)
        self.labelBuyInCost.Enable(enable)
        self.labelVictoryPoints.Enable(enable)
        self.labelAllowedNumbers.Enable(enable)

    def clear_play_area_checkboxes(self):
        for item in self.gameCheckBoxes:
            item.SetValue(False)

    def enable_disable_game_text_fields_by_score(self):
        """ Enables or disables game text fields according to how many points you have """
        for level in range(LEVELS):
            if self.settings.score >= self.settings.buy_in_for_level[level]:
                checkbox_enable = True
            # If checkbox is checked then item must be enabled
            # - must be able to un-check!
            elif self.gameCheckBoxes[level].IsChecked():
                checkbox_enable = True
            else:
                checkbox_enable = False

            if self.gameCheckBoxes[level].IsChecked():
                textfield_enable = True
            else:
                textfield_enable = False

            self.gameCheckBoxes[level].Enable(checkbox_enable)
            self.gameTextFields[level].Enable(textfield_enable)

    def nick_field_change(self, event):
        """ Check whether nick field and nick in settings is the same. If they are, disable change nick button. """
        if self.textNick.Value == self.settings.nick:
            self.buttonChangeNick.Disable()
        else:
            self.buttonChangeNick.Enable()

    def text_rounds_changed(self, event):
        log(f"MAIN", f"rounds setting changed to '{self.textRounds.Value}'")
        try:
            rounds = int(self.textRounds.Value)
            if rounds > 0:
                self.textRounds.SetForegroundColour(wx.BLACK)
                # Hack to get new color to apply right away - guess there is a better way but don't know it
                self.textRounds.Disable()
                self.textRounds.Enable()
                self.textRounds.SetFocus()
                self.settings.rounds = rounds
                self.buttonStart.Enable()
            else:
                raise ValueError
        except ValueError:
            self.textRounds.SetForegroundColour(wx.RED)
            self.buttonStart.Enable(False)

    def game_checkbox_checked(self, event):
        level = self.CHECKBOX_IDS.index(event.EventObject.Id)
        checked = event.EventObject.IsChecked()
        buy_in_points = self.settings.buy_in_for_level[level]

        log(f"MAIN", f"game checkbox checked - level {level} - {checked}")

        if checked:
            # Jos oli rittävästi pisteitä checkaamiseen
            if self.settings.score >= buy_in_points:
                self.update_score(self.settings.score - buy_in_points)

        else:  # unchecked
            self.update_score(self.settings.score + buy_in_points)

        self.enable_disable_game_text_fields_by_score()

        self.game_update_action(ACTION_CLICK)

    def game_text_field_changed(self, event):
        level = self.TEXT_FIELD_IDS.index(event.EventObject.Id)
        log(f"MAIN", f"level {level} text field set to {event.EventObject.Value}")

        ##################################
        # Highlight value if not allowed #
        ##################################
        value_correct = True

        # Value not correct if can't convert to int
        try:
            value = int(event.EventObject.Value)
        except ValueError:
            value_correct = False
            value = self.settings.min_allowed_guess

        # Value not correct if outside allowed range
        if value > self.settings.max_allowed_guess:
            value_correct = False
        elif value < self.settings.min_allowed_guess:
            value_correct = False

        if not value_correct:
            event.EventObject.SetForegroundColour(wx.Colour(255, 0, 0))
        else:
            event.EventObject.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWTEXT))

        self.game_update_action(ACTION_TYPE)
        # log(dir(event.EventObject))

    def game_hover(self, event):
        self.game_update_action(ACTION_HOVER)

    def game_hover_end(self, event):
        self.game_update_action(ACTION_HOVER_END)

    def game_update_action(self, action):
        # log(f"MAIN", f"in game_update_action: new action {action}, old action {self.user_action}")
        now = time.time()


        # Jos uudella actionilla on isompi prioriteetti niin muutetaan actionin tilaa
        if action >= self.user_action:
            self.user_action = action
            self._user_action_last_update = now
            # log(f"MAIN", f"user action set to {self.user_action}: {ACTION_TO_TEXT[self.user_action]}")

    def update_allowed_numbers_text(self):
        ming = self.settings.min_allowed_guess
        maxg = self.settings.max_allowed_guess

        self.labelAllowedNumbers.Label = f"Sallitut numerot: {ming} - {maxg}"
        for level in range(LEVELS):
            self.gameInfoAllowedNumbers[level].Label = f"({ming} - {maxg})"

    def update_gui(self, event):
        """
        Tätä kutsutaan x ms:n välein - tällä hetkellä hoitaa pelaajien actionien lähetyksen mutta
        olisi tarkoitus käyttää myös timerin nätimpään näyttämiseen
        """
        # Jos roundi käynnissä niin lähetetään action infoa serverille aina kun update_gui firetään
        if self.round_state == ROUND_IN_PROGRESS:
            # Jos action on vanhentunut niin nollataan
            if time.time() - self._user_action_last_update > self.settings.action_lifetime_ms / 1000:
                # log(f"MAIN", f"action expired")
                self.user_action = ACTION_NONE

            self.send_to_client(action='user_action', data=self.user_action)

    def quit(self, event):
        # Lopetetaan taustathreadit
        self.kill_server_thread()
        self.kill_client_thread()
        sys.exit(0)


if __name__ == '__main__':
    run()

