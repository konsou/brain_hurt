PRINT_LOG_ENTRIES = True
LOG_FILE = "program_log.txt"
CRASHDUMP_FILE = "crash_log.txt"

PORT = 7350
LEVELS = 5  # Changing this doesn't magically add levels - must implement in other places as well
SERVER_ACTION_UPDATE_INTERVAL_MS = 100


INFO_CLIENT_TERMINATED, INFO_SERVER_TERMINATED, COMMAND_KILL_CLIENT, COMMAND_KILL_SERVER = range(4)
STATE_LOCAL_IN_MENU, STATE_JOINED, STATE_HOSTED = range(3)
ROUND_NOT_STARTED, ROUND_PRE_COUNTDOWN, ROUND_IN_PROGRESS, ROUND_FINISHED = range(4)

# Järjestys on samalla prioriteettijärjestys näyttämisessä
ACTION_NONE, ACTION_HOVER_END, ACTION_HOVER, ACTION_CLICK, ACTION_TYPE = range(5)
ACTION_TO_TEXT = [
    "miettii...",
    "pohtii...",
    "harkitsee klikkaamista...",
    "klikkasi!",
    "kirjoittaa!"
]