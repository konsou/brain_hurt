import datetime
from constants import *
from settings import Settings

settings = Settings()

def log(tag, message, print_as_well=PRINT_LOG_ENTRIES):
    time_str = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    message = f"{time_str} - {tag}: {message}"
    if print_as_well:
        print(message)
    if settings.logging:
        with open(LOG_FILE, 'a') as f:
            f.write(f"{message}\n")

