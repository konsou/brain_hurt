import wx
import time
import queue
from PodSixNet.Connection import connection, ConnectionListener
from constants import *
from logger import log

TAG = 'CLIENT'


class Client(ConnectionListener):
    def __init__(self, host, port, main_window, message_queue):
        self.host = host
        self.port = port
        self.main_window = main_window
        self.message_queue = message_queue  # listens to messages from main thread
        self.killed_by_main = False
        self.running = False

    def Loop(self):
        log(TAG, "running")
        self.running = 1
        self.Connect((self.host, self.port))
        connection.Send({'action': 'nick', 'nick': self.main_window.settings.nick})

        while self.running:
            try:
                message = self.message_queue.get(block=False)
                self.handle_message_from_main(message)
            except queue.Empty:
                pass

            connection.Pump()
            self.Pump()
            time.sleep(0.01)

        log(TAG, "terminated")
        if not self.killed_by_main:
            self.send_to_main(action='thread_info', data=INFO_CLIENT_TERMINATED)

    def Network_nick_list(self, data):
        # log(TAG, f"got new nick list: {data}")
        self.send_to_main(action='nick_list', data=data['nick_list'])

    def Network_score(self, data):
        """ Updates own score from server """
        log(TAG, f"got own score from server: {data['score']}")
        self.send_to_main(action='score', data=data['score'])

    def Network_round_state(self, data):
        log(TAG, f"got round state info: {data}")
        self.send_to_main(action='round_state', data=data['round_state'])

    def Network_round_number(self, data):
        log(TAG, f"got new round number: {data}")
        self.send_to_main(action='round_number', data=data['round_number'])

    def Network_round_results(self, data):
        log(TAG, f"got round results: {data}")
        self.send_to_main(action='round_results', data=data['round_results'])

    def Network_final_results(self, data):
        log(TAG, f"got final game results: {data}")
        self.send_to_main(action='final_results', data=data['final_results'])

    def Network_server_disconnected(self, data):
        self.Network_disconnected(data)

    def Network_settings(self, data):
        log(TAG, f"got game settings: {data}")
        self.send_to_main(action='settings', data=data['settings'])

    ########################
    # built in stuff
    def Network_connected(self, data):
        log(TAG, "now connected to the server")

    def Network_error(self, data):
        log(TAG, f"error: {data['error'][1]} - closing connection")
        connection.Close()
        self.running = 0

    def Network_disconnected(self, data):
        log(TAG, f'Server disconnected')
        self.running = 0
    # built in stuff end
    ########################

    def send_to_main(self, action='message', data=''):
        wx.CallAfter(self.main_window.message_receiver, {action: data})

    def handle_message_from_main(self, message):
        if 'command' in message:
            if message['command'] == COMMAND_KILL_CLIENT:
                log(TAG, f"terminating (command from main)")
                self.killed_by_main = True
                self.running = False
        elif 'nick' in message:
            send_dict = {'action': 'nick', 'nick': message['nick']}
            log(TAG, f"send to server: {send_dict}")
            connection.Send(send_dict)
        elif 'round_results' in message:
            send_dict = {'action': 'round_results', 'round_results': message['round_results']}
            log(TAG, f"send to server: {send_dict}")
            connection.Send(send_dict)
        elif 'user_action' in message:
            send_dict = {'action': 'user_action', 'user_action': message['user_action']}
            # log(TAG, f"send to server: {send_dict}")
            connection.Send(send_dict)
        else:
            log(TAG, f"unknown message from main: {message}")


if __name__ == '__main__':
    client = Client('localhost', PORT)
    client.Loop()

