# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version May 29 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

NICK_TXT = 1000
TIMER_1S = 1001
TIMER_ROUND = 1002
GUI_UPDATE = 1003
ACTION_CHECK_TIMER = 1004


###########################################################################
## Class mainMenuFrame
###########################################################################

class mainMenuFrame(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"Brain Hurt", pos=wx.DefaultPosition,
                          size=wx.Size(921, 850), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.Size(950, 850), wx.DefaultSize)
        self.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString))

        gbSizer1 = wx.GridBagSizer(0, 0)
        gbSizer1.SetFlexibleDirection(wx.BOTH)
        gbSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.labelOwnIPStatic = wx.StaticText(self, wx.ID_ANY, u"Oma IP:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.labelOwnIPStatic.Wrap(200)
        self.labelOwnIPStatic.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelOwnIPStatic, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.labelOwnIP = wx.StaticText(self, wx.ID_ANY, u"(oma IP tähän)", wx.DefaultPosition, wx.DefaultSize, 0)
        self.labelOwnIP.Wrap(-1)
        self.labelOwnIP.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString))

        gbSizer1.Add(self.labelOwnIP, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.labelNick = wx.StaticText(self, wx.ID_ANY, u"Nick:", wx.Point(0, 1), wx.DefaultSize, 0)
        self.labelNick.Wrap(-1)
        self.labelNick.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))
        self.labelNick.SetMinSize(wx.Size(150, -1))

        gbSizer1.Add(self.labelNick, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.textNick = wx.TextCtrl(self, NICK_TXT, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.textNick.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))
        self.textNick.SetMinSize(wx.Size(250, -1))

        gbSizer1.Add(self.textNick, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.EXPAND, 5)

        self.buttonChangeNick = wx.Button(self, wx.ID_ANY, u"Vaihda nick", wx.DefaultPosition, wx.DefaultSize, 0)
        self.buttonChangeNick.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))
        self.buttonChangeNick.SetMinSize(wx.Size(150, -1))

        gbSizer1.Add(self.buttonChangeNick, wx.GBPosition(1, 2), wx.GBSpan(1, 1), wx.ALL | wx.EXPAND, 5)

        self.labelServerAddress = wx.StaticText(self, wx.ID_ANY, u"Serverin osoite:", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        self.labelServerAddress.Wrap(-1)
        self.labelServerAddress.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelServerAddress, wx.GBPosition(2, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                     5)

        self.textServerAddress = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.textServerAddress.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString))

        gbSizer1.Add(self.textServerAddress, wx.GBPosition(2, 1), wx.GBSpan(1, 1), wx.ALL | wx.EXPAND, 5)

        self.buttonConnect = wx.Button(self, wx.ID_ANY, u"Yhdistä", wx.DefaultPosition, wx.DefaultSize, 0)
        self.buttonConnect.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))

        gbSizer1.Add(self.buttonConnect, wx.GBPosition(2, 2), wx.GBSpan(1, 1), wx.ALL | wx.EXPAND, 5)

        self.labelRounds = wx.StaticText(self, wx.ID_ANY, u"Roundeja:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.labelRounds.Wrap(-1)
        self.labelRounds.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelRounds, wx.GBPosition(3, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.textRounds = wx.TextCtrl(self, wx.ID_ANY, u"5", wx.DefaultPosition, wx.DefaultSize, 0)
        self.textRounds.SetMaxLength(2)
        self.textRounds.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.textRounds, wx.GBPosition(3, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.buttonHost = wx.Button(self, wx.ID_ANY, u"Hosti peli", wx.DefaultPosition, wx.DefaultSize, 0)
        self.buttonHost.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))

        gbSizer1.Add(self.buttonHost, wx.GBPosition(3, 2), wx.GBSpan(1, 1), wx.ALL | wx.EXPAND, 5)

        multiListChoices = []
        self.multiList = wx.ListBox(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, multiListChoices, 0)
        self.multiList.SetMinSize(wx.Size(300, 500))

        gbSizer1.Add(self.multiList, wx.GBPosition(1, 4), wx.GBSpan(30, 2), wx.ALL | wx.EXPAND, 3)

        self.labelPlayers = wx.StaticText(self, wx.ID_ANY, u"Pelaajat:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.labelPlayers.Wrap(-1)
        self.labelPlayers.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelPlayers, wx.GBPosition(0, 4), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_BOTTOM, 5)

        self.m_staticline1 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        self.m_staticline1.SetMinSize(wx.Size(600, 5))

        gbSizer1.Add(self.m_staticline1, wx.GBPosition(4, 0), wx.GBSpan(1, 3), wx.EXPAND | wx.ALL, 5)

        self.buttonStart = wx.Button(self, wx.ID_ANY, u"Aloita 1. kierros", wx.DefaultPosition, wx.DefaultSize, 0)
        self.buttonStart.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))
        self.buttonStart.Enable(False)

        gbSizer1.Add(self.buttonStart, wx.GBPosition(5, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        self.labelTimeLeft = wx.StaticText(self, wx.ID_ANY, u"Roundi () alkaa: 30", wx.DefaultPosition, wx.DefaultSize,
                                           wx.ALIGN_CENTRE)
        self.labelTimeLeft.Wrap(-1)
        self.labelTimeLeft.SetFont(
            wx.Font(15, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelTimeLeft, wx.GBPosition(6, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.labelBuyInCost = wx.StaticText(self, wx.ID_ANY, u"Osallistumisen hinta", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.labelBuyInCost.Wrap(-1)
        self.labelBuyInCost.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelBuyInCost, wx.GBPosition(8, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.labelAllowedNumbers = wx.StaticText(self, wx.ID_ANY, u"Sallitut numerot: 1 - 10", wx.DefaultPosition,
                                                 wx.DefaultSize, 0)
        self.labelAllowedNumbers.Wrap(-1)
        self.labelAllowedNumbers.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelAllowedNumbers, wx.GBPosition(8, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.labelPoints = wx.StaticText(self, wx.ID_ANY, u"OMAT PISTEET: ()", wx.DefaultPosition, wx.DefaultSize, 0)
        self.labelPoints.Wrap(-1)
        self.labelPoints.SetFont(
            wx.Font(14, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelPoints, wx.GBPosition(7, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.labelVictoryPoints = wx.StaticText(self, wx.ID_ANY, u"Voitosta pisteitä", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        self.labelVictoryPoints.Wrap(-1)
        self.labelVictoryPoints.SetFont(
            wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

        gbSizer1.Add(self.labelVictoryPoints, wx.GBPosition(8, 2), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_HORIZONTAL,
                     5)

        self.m_staticline5 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        gbSizer1.Add(self.m_staticline5, wx.GBPosition(9, 0), wx.GBSpan(1, 3), wx.EXPAND | wx.ALL, 5)

        self.SetSizer(gbSizer1)
        self.Layout()
        self.timer1S = wx.Timer()
        self.timer1S.SetOwner(self, TIMER_1S)
        self.timerRound = wx.Timer()
        self.timerRound.SetOwner(self, TIMER_ROUND)
        self.timerGuiUpdate = wx.Timer()
        self.timerGuiUpdate.SetOwner(self, GUI_UPDATE)
        self.timerCheckAction = wx.Timer()
        self.timerCheckAction.SetOwner(self, ACTION_CHECK_TIMER)

        self.Centre(wx.BOTH)

        # Connect Events
        self.Bind(wx.EVT_CLOSE, self.quit)
        self.textNick.Bind(wx.EVT_TEXT, self.nick_field_change)
        self.buttonChangeNick.Bind(wx.EVT_BUTTON, self.change_nick)
        self.buttonConnect.Bind(wx.EVT_BUTTON, self.connect)
        self.textRounds.Bind(wx.EVT_TEXT, self.text_rounds_changed)
        self.buttonHost.Bind(wx.EVT_BUTTON, self.host_game)
        self.buttonStart.Bind(wx.EVT_BUTTON, self.start_round_button_pressed)
        self.Bind(wx.EVT_TIMER, self.timer_1s_tick, id=TIMER_1S)
        self.Bind(wx.EVT_TIMER, self.timer_round_tick, id=TIMER_ROUND)
        self.Bind(wx.EVT_TIMER, self.update_gui, id=GUI_UPDATE)
        self.Bind(wx.EVT_TIMER, self.game_action_expired, id=ACTION_CHECK_TIMER)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def quit(self, event):
        event.Skip()

    def nick_field_change(self, event):
        event.Skip()

    def change_nick(self, event):
        event.Skip()

    def connect(self, event):
        event.Skip()

    def text_rounds_changed(self, event):
        event.Skip()

    def host_game(self, event):
        event.Skip()

    def start_round_button_pressed(self, event):
        event.Skip()

    def timer_1s_tick(self, event):
        event.Skip()

    def timer_round_tick(self, event):
        event.Skip()

    def update_gui(self, event):
        event.Skip()

    def game_action_expired(self, event):
        event.Skip()


