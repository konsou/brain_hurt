import gui
import wx
import settings
from constants import *

settings_ = settings.Settings()
score_calculator = settings_.score_for_level
buy_in_calculator = settings_.buy_in_for_level

START_ROW = 10
START_COLUMN = 0
COLUMN_WIDTHS = [100, 350, 100]


class GuiPart2(gui.mainMenuFrame):
    """ Adds game controls to the wxForms-generated GUI """
    def __init__(self, parent):
        gui.mainMenuFrame.__init__(self, parent)

        self.TEXT_FIELD_IDS = [2000, 2001, 2002, 2003, 2004]
        self.CHECKBOX_IDS =   [2100, 2101, 2102, 2103, 2104]
        self.gameInfoLabels = []
        self.gameInfoAllowedNumbers = []
        self.gameScoreLabels = []
        self.gameCheckBoxes = []
        self.gameTextFields = []
        self.gameSpacerLines = []

        for level in range(LEVELS):
            if level == 0:
                self.gameInfoLabels.append(wx.StaticText(self, wx.ID_ANY,
                                                         "Suurin numero, jota kukaan muu ei ole arvannut:",
                                                         wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE))
            else:
                self.gameInfoLabels.append(wx.StaticText(self, wx.ID_ANY,
                                                         f"Edellisen tason suurin ei-voittanut arvaus:",
                                                         wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE))
            self.gameInfoLabels[level].Wrap(COLUMN_WIDTHS[1])

            self.gameInfoAllowedNumbers.append(wx.StaticText(self, wx.ID_ANY, "(1 - 2)",
                                                             wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE))

            self.gameCheckBoxes.append(wx.CheckBox(self, self.CHECKBOX_IDS[level], f"{buy_in_calculator[level]}",
                                                   wx.DefaultPosition, wx.DefaultSize, 0))

            self.gameTextFields.append(wx.TextCtrl(self, self.TEXT_FIELD_IDS[level], "1",
                                                   wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE | wx.SIMPLE_BORDER))
            self.gameTextFields[level].SetMaxSize(wx.Size(35, 28))
            self.gameTextFields[level].SetFont(wx.Font(15, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL,
                                                       wx.FONTWEIGHT_BOLD, False, wx.EmptyString))

            self.gameScoreLabels.append(wx.StaticText(self, wx.ID_ANY,
                                                      f"{score_calculator[level]}",
                                                      wx.DefaultPosition, wx.DefaultSize, 0))

            self.gameSpacerLines.append(wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL))

            self.Sizer.Add(self.gameInfoLabels[level], wx.GBPosition(level * 4 + START_ROW, START_COLUMN + 1),
                           wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
            self.Sizer.Add(self.gameInfoAllowedNumbers[level], wx.GBPosition(level * 4 + START_ROW + 1, START_COLUMN + 1),
                           wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 0)
            self.Sizer.Add(self.gameCheckBoxes[level], wx.GBPosition(level * 4 + START_ROW + 2, START_COLUMN),
                           wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
            self.Sizer.Add(self.gameTextFields[level], wx.GBPosition(level * 4 + START_ROW + 2, START_COLUMN + 1),
                           wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
            self.Sizer.Add(self.gameScoreLabels[level], wx.GBPosition(level * 4 + START_ROW + 2, START_COLUMN + 2),
                           wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
            self.Sizer.Add(self.gameSpacerLines[level], wx.GBPosition(level * 4 + START_ROW + 3, START_COLUMN),
                           wx.GBSpan(1, 3), wx.EXPAND | wx.ALL, 5)

            # Events
            self.gameTextFields[level].Bind(wx.EVT_TEXT, self.game_text_field_changed)
            self.gameTextFields[level].Bind(wx.EVT_ENTER_WINDOW, self.game_hover)
            self.gameTextFields[level].Bind(wx.EVT_LEAVE_WINDOW, self.game_hover_end)
            # self.gameTextFields[level].Bind(wx.EVT_LEFT_DOWN, self.game_click)
            self.gameCheckBoxes[level].Bind(wx.EVT_CHECKBOX, self.game_checkbox_checked)
            self.gameCheckBoxes[level].Bind(wx.EVT_ENTER_WINDOW, self.game_hover)
            self.gameCheckBoxes[level].Bind(wx.EVT_LEAVE_WINDOW, self.game_hover_end)
            # self.gameCheckBoxes[level].Bind(wx.EVT_LEFT_DOWN, self.game_click)

    def game_text_field_changed(self, event):
        pass

    def game_checkbox_checked(self, event):
        pass

    def game_hover(self, event):
        pass

    def game_hover_end(self, event):
        pass
